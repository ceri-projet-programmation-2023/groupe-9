# README Mathéo FAUCHEZ Section
## Responsible for:
### 1.4 Menu
#### 1.4.1 Display a help button explaining how to play
#### 1.4.2 Display a button to restart a game
#### 1.4.3 Display a button to get a hint
#### 1.4.4 Display a button to return to the main menu
### 1.5 Scores
#### 1.5.1 Display the elapsed time since the start of the game
#### 1.5.2 Display the current winning streak
#### 1.5.3 Button to display the best scores
#### 1.5.4 Save the best scores in a file

# Work History
> **24/10/2023:** Initial "clean" code, completion of tests, and introduction to JavaFX. Implementation of a constructor for menu buttons to ensure button uniformity, addition of a singleton class for the chronometer, display not automatically updated.

> **26/10/2023:** Added automatic refresh every second for the chronometer in the application.

> **27/10/2023:** Added a score manager that allows recording scores.

> **13/01/2024:** Created the Javadoc.
