# Steps 2 and 3 - Installing a remote HTTP server    
We're going to use the flask framework to install an HTTP server on my remote server with this IP address: 51.178.36.249 which corresponds to the url address https://olivier-fabre.com  
The administration part of the Plesk server can be accessed at the following address: https://51.178.36.249:8443  
  
  
access to the http server will be from the end-point: https://olivier-fabre.com:5000/similaire/  
  
  
## Flask installation step (http server):  
  
Flask supports Python 3.8 and newer  
see python version > python3 --version  
  
1/ From the olivier-fabre.com domain name management, go to /ofcom/  
2/ Create a directory /similar/ from the "files" interface  
3/ Go to the ssh root and issue the following command  
> cd /var/www/vhosts/  
> cd olivier-fabre.com  
> cd ofcom  
> cd similar  
> apt-get install python3-venv  
> python3 -m venv .venv  
> .venv/bin/activate  
  
  
4/ go to ssh ofcom and run the following command  
> pip3 install Flask


Check that it is correctly installed  
> pip list | grep Flask  
  
  
5/ Install the https keys to take them into account on Flask  
the fullchain.pem (certificate) and privkey.pem (secret keys) are on /usr/local/psa/var/modules/letsencrypt/etc/live/olivier-fabre.com/  
simply insert them into the Flask code as > app.run(ssl_content)  
  
5/ Create an app.py file  
and add the following test code (take care with indentation in python):

```bash
from flask import Flask  

app = Flask(__name__)

@app.route('/similaire/')
def similaire():
    return 'Page de test pour Similaire'

if __name__ == '__main__':
    app.run(ssl_context=('/usr/local/psa/var/modules/letsencrypt/etc/live/olivier-fabre.com/fullchain.pem',
                         '/usr/local/psa/var/modules/letsencrypt/etc/live/olivier-fabre.com/privkey.pem'),
            host='0.0.0.0', port=5000)
```

6/ Run the following command to launch the http server > python3 app.py  
7/ Go to the following url: https://olivier-fabre.com:5000/similaire/   
If the screen displays: Test page for Similar -> then the server is properly installed and working  
  
  
## Model installation step   
1/ Create a model directory at the root of the /similar/ directory  
> mkdir model   


2/ Download the pre-trained Word2vec FR model named frWac_non_lem_no_postag_no_phrase_200_skip_cut100.bin  
 at this address: https://e-uapv2023.univ-avignon.fr/mod/folder/view.php?id=54500    


3/ Upload it to the server in /model/    




## Installation step Python3.9 + Pip SSL + BZ2  
1/ First we need to make sure we have at least Python 3.9 because Gensim only works on =>Python3.9 We're going to install the python repository and the difficulty is the pipe command, which we need to make sure accepts the SLL environment.  
If this is not the case, as it was initially for me, then do the commands below (note that I am in Debian)  
> curl -O https://www.python.org/ftp/python/3.9.18/Python-3.9.18.tgz  
> tar xzf Python-3.9.18.tgz  
a folder has been created /Python-3.9.18/  
> cd Python-3.9.18  
> ./configure --enable-optimizations  
Attention if you have 4 cores on your server then update the last index below with your nbr of cores  
> make -j 4  
> make altinstall  
Check the installation with the command > python3.9 --version  
Then to set Pyhton3.9 as default  
> update-alternatives --install /usr/bin/python3 python3 /usr/local/bin/python3.9 1  
> update-alternatives --config python3  
> python3 --version  
> curl https://bootstrap.pypa.io/get-pip.py -o get-pip.py  
> python3 get-pip.py  
> deactivate  
> rm -rf /var/www/vhosts/olivier-fabre.com/ofcom/similaire/.venv  
> python3.9 -m venv /var/www/vhosts/olivier-fabre.com/ofcom/similaire/.venv  
> source /var/www/vhosts/olivier-fabre.com/ofcom/similaire/.venv/bin/activate  
> pip --version  
  
> apt-get install libssl-dev  
> cd Python-3.9.18  
> ./configure --enable-optimizations  
> make -j 4 <-- I have 4 cores  
> make altinstall  
Check the version of pip SSL  
> python3.9 -c "import ssl; print(ssl.OPENSSL_VERSION)"  
If problem re install pip  
>curl https://bootstrap.pypa.io/get-pip.py -o get-pip.py  
> python3.9 get-pip.py  
  
Install BZ2 dependencies    
> cd Python-3.9.18  
> apt-get install libbz2-dev  
> make clean  
> ./configure --enable-optimizations  
> make -j 4  
> make altinstall  
Checking the installation  
> python3.9 -c "import bz2; print(bz2.__doc__)"

## Gensim installation step  
1/ Before testing, you need to install Gensim  
It is strongly recommended that you always use Gensim in a virtual environment, which is why you create a .venv directory.  
> python3.9 install gensim <-- no longer pip3 but python3.9 because pyhton version 3.9 is the default.  
  
2/ Check that Gensim has been installed correctly  
Create a Gensim.py file  
Add the following code:  


```bash
import gensim
print(gensim.__version__)
```   
make sure the virtual environment is activated  
> .venv/bin/activate  
move to the root  
> python3.9 Gensim.py 
  
  
## Test using Word2vec and Gsim  


1/ Create a python file named testSimilaire.py  
We're going to do a test to find 5 words similar to "avion", which is part of the dictionary of mystery words.  
To do this, we're going to place the code below:  
2/ Place the following code:  
  
```bash
from gensim.models import KeyedVectors


# Path to our Word2Vec model
model_path = '/model/frWac_non_lem_no_postag_no_phrase_200_skip_cut100.bin'


# Load the model
model = KeyedVectors.load_word2vec_format(model_path, binary=True)


# Find and display the 5 words most similar to "aircraft".
similar_words = model.most_similar('avion', topn=5)
print(similar_words)  
```

3/ Test the python script on ssh root  
make sure the virtual environment is activated  
> .venv/bin/activate   
go to root  
> python3 testSimilaire.py  
You should get the following results:  


```bash
root@myserver:/var/www/vhosts/olivier-fabre.com/ofcom/similaire# python3.9 testSimilaire.py
[('flights', 0.7734482288360596), ('landing', 0.7604251503944397), ('take-off', 0.7471811175346375), ('charter', 0.7422746419906616), ('long-haul', 0.7390015721321106)]
``` 


## API application step    
  
1/ Create a python file called similar.py    
Add the following code     
  
```bash
from flask import Flask, request, jsonify
from gensim.models import KeyedVectors


app = Flask(__name__)


# Load the Word2Vec model
model = KeyedVectors.load_word2vec_format('model/en_model_skip.bin', binary=True)


@app.route('/similar', methods=['GET']) # Modify as GET
def get_similar_words():
    word = request.args.get('word', '') # Retrieve the word from the request parameters
    print(f "Word received: {word}") # Log for debugging
    if not word:
        return "No word provided", 400
    try:
        similar_words = model.most_similar(word, topn=5)
        similar_words_str = ', '.join([word for word, _ in similar_words])
        return similar_words_str
    except KeyError as e:
        return str(e), 404


if __name__ == '__main__':
    app.run(ssl_context=('/usr/local/psa/var/modules/letsencrypt/etc/live/olivier-fabre.com/fullchain.pem',
                         /usr/local/psa/var/modules/letsencrypt/etc/live/olivier-fabre.com/privkey.pem'),
            host='0.0.0.0', port=5000)
    
```   
  
## Code explanation:  
  
```   
Flask is imported to create the web application.
request is used to access the request data sent to the Flask application.
KeyedVectors is a Gensim class used to work with Word2Vec templates.
  
We create an instance of the Flask application called app  
  
Load the model and binary=True indicates that the file is in binary format  
  
@app.route('/similar', methods=['GET']) defines a route (an API endpoint) to which GET requests can be sent.
The get_similar_words() function processes GET requests to this route.


word = request.args.get('word', '') retrieves the word for which to find similar words from the URL parameters. If no word is provided, it uses an empty string by default.
If no word is provided (word is empty), the function returns an error message with HTTP status code 400 (Bad Request).
similar_words = model.most_similar(word, topn=5) uses the Word2Vec model to find the 5 most similar words.
The response is a string of similar words, separated by commas.
If the word is not found in the template (KeyError), the function returns an error message with the HTTP status code 404 (Not Found).


ssl_context specifies the paths to the SSL certificate and private key files to enable the application to communicate using HTTPS.
host='0.0.0.0' indicates that the application can be accessed on all the host's network interfaces.
port=5000 defines the port on which the application listens.


In summary, this Flask script creates a web API that accepts GET requests containing a word and returns the 5 most similar words according to a Word2Vec model. The API is served using HTTPS with the specified SSL certificates.


```   


Link: https://gitlab.com/ceri-projet-programmation-2023/groupe-9/-/tree/partie_python/ServeurHTTP

# Step 4 - Coding the game in Java  


The game application coded in Java SDK 10 must make a GET request to my http server and receive the result in raw format, displaying the results on the screen output.   


Creating the SimilarWordsClient() class  



```java
import java.net.HttpURLConnection;
import java.net.URL;
import java.io.BufferedReader;
import java.io.InputStreamReader;


/**
 * Client class for sending a GET request to a server and reading the response.
 * This class is specifically used to send a word to a Flask server and receive similar words back.
 * server and receive similar words in return.
 */




public class SimilarWordsClient {








    /**
     * Main entry point to the program.
     *
     * @param args The command line arguments (not used).
     */
    public static void main(String[] args) {
        try {
            // Build the URL with a request parameter
            String wordToSend = "avion"; // Replace with the word of your choice
            URL url = new URL("https://olivier-fabre.com:5000/similaire?word=" + wordToSend);


            // Open the connection
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("GET");


            // Read the response
            try (BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream(), "utf-8")) {
                StringBuilder response = new StringBuilder();
                String responseLine = null;
                while ((responseLine = br.readLine()) != null) {
                    response.append(responseLine.trim());
                }
                System.out.println(response.toString());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}






```  
  
Details of SimilarWordsClient() class  


```  
The SimilarWordsClient class is a Java program which sends a GET request to a specified URL and prints the response.  
The main method is the program's entry point.  
import java.net.HttpURLConnection;
import java.net.URL;
import java.io.BufferedReader;
import java.io.InputStreamReader;
These imports allow you to use the classes needed to manage HTTP connections, read and write data on these connections, and read buffered data.
wordToSend is the variable containing the word to be sent to the server.
The Flask server URL is constructed by adding this word as a request parameter. This URL points to a Flask API which expects a word as a parameter in order to return similar words.


An HttpURLConnection is opened at the specified URL.
The request method is set to GET, which means that the program requests data from the server.
A BufferedReader is used to read the server response line by line.
The response is constructed and accumulated in a StringBuilder.
The complete response is finally printed in the console.
This part of the code catches any exceptions that may occur during the execution of the program, such as connection problems or errors when reading or writing data.
This program is used to interact with a Flask API that takes a word as input and returns similar words. It can be used to test the API or to integrate similar word search functionality into a Java application.


```  






# Step 5 - Script improvements  


## Improve similar.py -> similar2.py    


During my tests, I noticed a first problem: for the word 'table', 'tableau' we had in the list of similar words returned: 'tables', 'tableaux'.  
I had to modify the similar.py script so as not to send the original plural word in the list of 5 similar words.  
This new version can be found in the file similar2.py, the code of which is shown below:

```bash
from flask import Flask, request, jsonify
from gensim.models import KeyedVectors

app = Flask(__name__)

# Charger le modèle Word2Vec
model = KeyedVectors.load_word2vec_format('model/fr_model_skip.bin', binary=True)

@app.route('/similaire', methods=['GET'])
def get_similar_words():
    word = request.args.get('word', '')
    print(f"Mot reçu: {word}")
    if not word:
        return "Aucun mot fourni", 400

    try:
        similar_words = []
        topn = 5  # Nombre initial de mots similaires à chercher
        while len(similar_words) < 5:
            for similar_word, _ in model.most_similar(word, topn=topn):
                # Vérifier si le mot similaire n'est pas le mot d'origine au pluriel
                if similar_word != word + 's' and similar_word != word + 'x' and similar_word not in similar_words:
                    similar_words.append(similar_word)
                if len(similar_words) == 5:
                    break
            topn += 5  # Augmenter le nombre de mots à chercher si nécessaire

        similar_words_str = ', '.join(similar_words)
        return similar_words_str
    except KeyError as e:
        return str(e), 404

if __name__ == '__main__':
    app.run(ssl_context=('/usr/local/psa/var/modules/letsencrypt/etc/live/olivier-fabre.com/fullchain.pem',
                         '/usr/local/psa/var/modules/letsencrypt/etc/live/olivier-fabre.com/privkey.pem'),
            host='0.0.0.0', port=5000)


```

### Code explanations  

```
- similar_words = []: Initializes an empty list to store similar words.  
- topn = 5: Sets the initial number of similar words to search for.  
- while len(similar_words) < 5:: A loop that continues until 5 unique similar words are found.  
- for similar_word, _ in model.most_similar(word, topn=topn): Scans for words similar to the search word, obtained from the Word2Vec model (model.most_similar). topn specifies the number of results to return. 
- if similar_word != word + 's' and similar_word != word + 'x' and similar_word not in similar_words: Checks whether the similar word is not a plural form (addition of 's' or 'x') or already in the similar_words list.  
- similar_words.append(similar_word): Adds the similar word to the list if it passes the filter.
- if len(similar_words) == 5: break: Exits the loop if 5 unique words have been found.
- topn += 5: Increases the number of results to search for if fewer than 5 unique words have been found in the current iteration.


```


## Improvement on file similar2.py -> similar3.py   

I found a second problem: when searching for the word 'bateau', 'fete', the list of similar words included 'bâteau', 'fête'.  
I had to modify the script a second time so as not to return these words, but in the end I still sent the 5 most similar words.  
This new version can be found in the file similar2.py, the code of which is shown below:

```bash
from flask import Flask, request, jsonify
from gensim.models import KeyedVectors

app = Flask(__name__)

# Charger le modèle Word2Vec
model = KeyedVectors.load_word2vec_format('model/fr_model_skip.bin', binary=True)

import unicodedata

def remove_accents(input_str):
    nfkd_form = unicodedata.normalize('NFKD', input_str)
    return "".join([c for c in nfkd_form if not unicodedata.combining(c)])

@app.route('/similaire', methods=['GET'])
def get_similar_words():
    word = request.args.get('word', '')
    print(f"Mot reçu: {word}")
    if not word:
        return "Aucun mot fourni", 400

    try:
        similar_words = []
        topn = 5
        while len(similar_words) < 5:
            for similar_word, _ in model.most_similar(word, topn=topn):
                similar_word_no_accents = remove_accents(similar_word)

                # Vérifier si le mot similaire n'est ni une forme plurielle ni une variante accentuée du mot d'origine
                if similar_word_no_accents != remove_accents(word) \
                   and similar_word_no_accents != word + 's' \
                   and similar_word_no_accents != word + 'x' \
                   and similar_word_no_accents not in similar_words:
                    similar_words.append(similar_word_no_accents)
                if len(similar_words) == 5:
                    break
            topn += 5

        similar_words_str = ', '.join(similar_words)
        return similar_words_str
    except KeyError as e:
        return str(e), 404

if __name__ == '__main__':
    app.run(ssl_context=('/usr/local/psa/var/modules/letsencrypt/etc/live/olivier-fabre.com/fullchain.pem',
                         '/usr/local/psa/var/modules/letsencrypt/etc/live/olivier-fabre.com/privkey.pem'),
            host='0.0.0.0', port=5000)



```



### Code explanations  

```
remove_accents(input_str): is a Python function designed to remove accents from characters in a string




- import unicodedata: The unicodedata module provides access to the Unicode database, which is a standard for representing text in most of the world's writing systems. This module is used to work with the Unicode properties of characters.
- def remove_accents(input_str): Declares a function named remove_accents which takes an input_str parameter. This parameter represents the string from which accents will be removed.
- nfkd_form = unicodedata.normalize('NFKD', input_str): The character string is normalized using the NFKD normalization form (Normalization Form KD). NFKD normalisation breaks down each character into its canonical components. For example, an accented character such as 'é' will be broken down into 'e' and the separate accent.
- return "".join([c for c in nfkd_form if not unicodedata.combining(c)]): This line of code reconstructs a character string from the decomposed components, excluding combining characters (such as accents). The unicodedata.combining(c) function returns True if 'c' is a combining character (such as an accent), which means that it will be excluded from the final string. The result is a string without accents.




```




## Improvement on java file SimilarWordsClient.java -> SimilarWordsClient_Version2.java  


As you can see in the image below, special characters are sent incorrectly by my SimilarWordsClient class in java. Here, the word 'stoic'.  
![Texte alternatif](CaptureImage1.jpg "Titre facultatif")
I need to specify that the word is encoded in UTF-8 format before sending it to the HTTP server.
I need to modify the java code with a new version which can be found in SimilarWordsClient_Version2.java and the code is below:


```java
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.io.BufferedReader;
import java.io.InputStreamReader;


public class SimilarWordsClient {
    public static void main(String[] args) {
        try {
            String word = "party";
            // Encode the word to include it in the URL
            String encodedWord = URLEncoder.encode(word, "UTF-8");


            URL url = new URL("https://olivier-fabre.com:5000/similaire?word=" + encodedWord);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("GET");
            BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream(), "utf-8"));


            StringBuilder response = new StringBuilder();
            String line;
            while ((line = reader.readLine()) != null) {
                response.append(line.trim());
            }
            reader.close();


            System.out.println(response.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}


```




### Code explanations  





- The encoding for URLs is slightly different and requires the use of percent encoding. We add the java.net.URLEncoder.
- URLEncoder.encode(word, "UTF-8") is used to encode the word "party" for example in UTF-8 so that it is correctly transmitted via a URL


```

link: https://gitlab.com/ceri-projet-programmation-2023/groupe-9/-/tree/partie_python/java