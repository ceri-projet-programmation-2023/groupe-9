# Etape 2 et 3 - Installation d'un serveur Http à distance    
Nous allons utiliser le framework flask pour installer un serveur Http sur mon serveur distant avec cette adresse IP: 51.178.36.249 qui correspond à l'adresse url https://olivier-fabre.com  
La partie adminsitration du serveur Plesk est accessible à l'adresse suivante: https://51.178.36.249:8443  
  
  
l'accès au serveur http se fera à partir du end-point: https://olivier-fabre.com:5000/similaire/  
  
  
## Etape d'installation de Flask (serveur http):  
  
Flask supports Python 3.8 and newer  
voir version python > python3 --version  
  
1/ A partir de la gestion du nom de domaine olivier-fabre.com, Se rendre sur /ofcom/  
2/ Créer un répertoire /similaire/ à partir de l'interface "fichiers"  
3/ Se rendre sur le ssh root pour faire la commande suivante  
> cd /var/www/vhosts/  
> cd olivier-fabre.com  
> cd ofcom  
> cd similaire  
> apt-get install python3-venv  
> python3 -m venv .venv  
> . .venv/bin/activate  
  
  
4/ se rendre sur ssh ofcom et faire la commande suivante  
> pip3 install Flask

On verifie qu'il est bien installé  
> pip list | grep Flask  
  
  
5/ Installation des clefs https pour les prendre en compte sur Flask  
les clefs fullchain.pem (certificat) et privkey.pem (clefs secretes) sont sur /usr/local/psa/var/modules/letsencrypt/etc/live/olivier-fabre.com/  
il suffire de les incoporer dans le code Flask sous la forme > app.run(ssl_content)  
  
5/ Créer un fichier app.py  
et ajouter le code de test suivant (attention à l'indentation en python):   
  
```bash
from flask import Flask  

app = Flask(__name__)

@app.route('/similaire/')
def similaire():
    return 'Page de test pour Similaire'

if __name__ == '__main__':
    app.run(ssl_context=('/usr/local/psa/var/modules/letsencrypt/etc/live/olivier-fabre.com/fullchain.pem',
                         '/usr/local/psa/var/modules/letsencrypt/etc/live/olivier-fabre.com/privkey.pem'),
            host='0.0.0.0', port=5000)
```
  
6/ faite la commande suivante pour lancer le serveur http > python3 app.py  
7/ Aller sur l'url suivante: https://olivier-fabre.com:5000/similaire/   
Si l'ecran affiche: Page de test pour Similaire -> alors le serveur est bien installé et fonctionne  
  
  
## Etape d'installation du model   
1/ Créer un répertoire model à la racine du repertoire /similaire/  
> mkdir model   

2/ Télécharger le Modèle Word2vec FR pré-entrainé nommé frWac_non_lem_no_postag_no_phrase_200_skip_cut100.bin  
 à cette adresse: https://e-uapv2023.univ-avignon.fr/mod/folder/view.php?id=54500    

3/ L'uploader sur le serveur dans /model/    


## Etape d'installation Python3.9 + Pip SSL + BZ2  
1/ Avant on doit s'assurer que nous avons bien Python 3.9 minimum car Gensim ne fonction ne que sur =>Python3.9 On va installer le depot python et la difficulté est la commande pipe que nous devons nous assurer quelle accepte l'environement SLL  
Si ce n'est pas le cas comme initialement pour moi alors faites les commandes ci-dessous (attention je suis en Debian)  
> curl -O https://www.python.org/ftp/python/3.9.18/Python-3.9.18.tgz  
> tar xzf Python-3.9.18.tgz  
un dossier a été créé /Python-3.9.18/  
> cd Python-3.9.18  
> ./configure --enable-optimizations  
Attention si vous avez 4 coeurs sur votre serveur alors mettre à jour le dernier indice ci-dessous avce votre nbr de coeurs  
> make -j 4  
> make altinstall  
On vérifie l'installation avec la commande > python3.9 --version  
Ensuite pour mettre Pyhton3.9 par defaut  
> update-alternatives --install /usr/bin/python3 python3 /usr/local/bin/python3.9 1  
> update-alternatives --config python3  
> python3 --version  
> curl https://bootstrap.pypa.io/get-pip.py -o get-pip.py  
> python3 get-pip.py  
> deactivate  
> rm -rf /var/www/vhosts/olivier-fabre.com/ofcom/similaire/.venv  
> python3.9 -m venv /var/www/vhosts/olivier-fabre.com/ofcom/similaire/.venv  
> source /var/www/vhosts/olivier-fabre.com/ofcom/similaire/.venv/bin/activate  
> pip --version  
  
> apt-get install libssl-dev  
> cd Python-3.9.18  
> ./configure --enable-optimizations  
> make -j 4     <-- j'ai 4 coeurs  
> make altinstall  
On verifie la version de pip SSL  
> python3.9 -c "import ssl; print(ssl.OPENSSL_VERSION)"  
Si probleme re installer pip  
>curl https://bootstrap.pypa.io/get-pip.py -o get-pip.py  
> python3.9 get-pip.py  
  
On installe les dependances BZ2    
> cd Python-3.9.18  
> apt-get install libbz2-dev  
> make clean  
> ./configure --enable-optimizations  
> make -j 4  
> make altinstall  
Vérification de l'installation  
> python3.9 -c "import bz2; print(bz2.__doc__)"  
  
  
## Etape d'installation Gensim  
1/ avant de tester, on doit installer Gensim  
Attention, Il est conseillé fortement d'utilisé toujours Gensim sur un environnement virtuel en principe d'ou la création du répertore .venv  
> python3.9 install gensim  <-- on ne mets plus pip3 mais python3.9 car par defaut pyhton version 3.9  
  
2/ On teste si Gensim a bien été installé  
On créé un fichier Gensim.py  
On ajoute le code suivant:  

```bash
import gensim
print(gensim.__version__)
```   
on s'assure que l'environnement virtuel est bien activé  
> . .venv/bin/activate  
on se place à la racine  
> python3.9 Gensim.py 
  
  
## Etape de test d'utilisation de Word2vec et Gsim  

1/ Créer un fichier python nommé testSimilaire.py  
Nous allons faire un test pour trouver 5 mots similaires à "avion" qui fait parti du dictionnaire des mots mystères.  
Pour se faire nous allons placer le code ci-dessous:  
2/ placer le code suivant:  
  
```bash
from gensim.models import KeyedVectors

# Chemin vers notre modèle Word2Vec
model_path = '/model/frWac_non_lem_no_postag_no_phrase_200_skip_cut100.bin'

# Charger le modèle
model = KeyedVectors.load_word2vec_format(model_path, binary=True)

# Trouver et afficher les 5 mots les plus similaires à "avion"
similar_words = model.most_similar('avion', topn=5)
print(similar_words)  
```  
    
  
3/ Tester le script python sur ssh root  
on s'assure que l'environnement virtuel est bien activé  
> . .venv/bin/activate   
on se place à la racine  
>  python3 testSimilaire.py  
On devrait avoir les résultats suivants:  

```bash
root@myserver:/var/www/vhosts/olivier-fabre.com/ofcom/similaire# python3.9 testSimilaire.py
[('vols', 0.7734482288360596), ('atterrissage', 0.7604251503944397), ('décollage', 0.7471811175346375), ('charter', 0.7422746419906616), ('long-courrier', 0.7390015721321106)]
``` 

## Etape de l'application API    
  
1/ Créer un fichier python nommé similar.py    
Ajouter le code suivant     
  
```bash
from flask import Flask, request, jsonify
from gensim.models import KeyedVectors

app = Flask(__name__)

# Charger le modèle Word2Vec
model = KeyedVectors.load_word2vec_format('model/fr_model_skip.bin', binary=True)

@app.route('/similaire', methods=['GET'])  # Modifier en GET
def get_similar_words():
    word = request.args.get('word', '')  # Récupération du mot depuis les paramètres de requête
    print(f"Mot reçu: {word}")  # Log pour débogage
    if not word:
        return "Aucun mot fourni", 400
    try:
        similar_words = model.most_similar(word, topn=5)
        similar_words_str = ', '.join([word for word, _ in similar_words])
        return similar_words_str
    except KeyError as e:
        return str(e), 404

if __name__ == '__main__':
    app.run(ssl_context=('/usr/local/psa/var/modules/letsencrypt/etc/live/olivier-fabre.com/fullchain.pem',
                         '/usr/local/psa/var/modules/letsencrypt/etc/live/olivier-fabre.com/privkey.pem'),
            host='0.0.0.0', port=5000)
    
```   
  
## Explications du code:  
  
```   
Flask est importé pour créer l'application web.
request est utilisé pour accéder aux données de la requête envoyée à l'application Flask.
KeyedVectors est une classe de Gensim utilisée pour travailler avec les modèles Word2Vec.
  
On créé une instance de l'application Flask qui se nomme app  
  
On charge le model et binary=True indique que le fichier est au format binaire  
  
@app.route('/similaire', methods=['GET']) définit une route (un point de terminaison d'API) à laquelle les requêtes GET peuvent être envoyées.
La fonction get_similar_words() traite les requêtes GET à cette route.

word = request.args.get('word', '') récupère le mot pour lequel trouver des mots similaires à partir des paramètres de l'URL. Si aucun mot n'est fourni, il utilise une chaîne vide par défaut.
Si aucun mot n'est fourni (word est vide), la fonction renvoie un message d'erreur avec le code de statut HTTP 400 (Bad Request).
similar_words = model.most_similar(word, topn=5) utilise le modèle Word2Vec pour trouver les 5 mots les plus similaires.
La réponse est une chaîne de caractères composée des mots similaires, séparés par des virgules.
Si le mot n'est pas trouvé dans le modèle (KeyError), la fonction renvoie un message d'erreur avec le code de statut HTTP 404 (Not Found).

ssl_context spécifie les chemins vers les fichiers de certificat SSL et de clé privée pour permettre à l'application de communiquer en utilisant HTTPS.
host='0.0.0.0' indique que l'application est accessible sur toutes les interfaces réseau de l'hôte.
port=5000 définit le port sur lequel l'application écoute.

En résumé, ce script Flask crée une API web qui accepte des requêtes GET contenant un mot et renvoie les 5 mots les plus similaires selon un modèle Word2Vec. L'API est servie en utilisant HTTPS avec les certificats SSL spécifiés.

```   

Lien: https://gitlab.com/ceri-projet-programmation-2023/groupe-9/-/tree/partie_python/ServeurHTTP  


# Etape 4 - Codage de la partie en java  

L'application du jeu codée en Java SDK 10 doit réaliser une requête de type GET à mon serveur http et recevoir le résultat sous format brute et affiche les résultats sur la sortie écran.   

Création de la classe SimilarWordsClient()  

```java
import java.net.HttpURLConnection;
import java.net.URL;
import java.io.BufferedReader;
import java.io.InputStreamReader;

/**
 * Classe client pour envoyer une requête GET à un serveur et lire la réponse.
 * Cette classe est spécifiquement utilisée pour envoyer un mot à un serveur Flask
 * et recevoir des mots similaires en retour.
 */


public class SimilarWordsClient {




    /**
     * Point d'entrée principal du programme.
     *
     * @param args Les arguments de la ligne de commande (non utilisés).
     */
    public static void main(String[] args) {
        try {
            // Construire l'URL avec un paramètre de requête
            String wordToSend = "avion"; // Remplacer par le mot de votre choix
            URL url = new URL("https://olivier-fabre.com:5000/similaire?word=" + wordToSend);

            // Ouvrir la connexion
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("GET");

            // Lire la réponse
            try (BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream(), "utf-8"))) {
                StringBuilder response = new StringBuilder();
                String responseLine = null;
                while ((responseLine = br.readLine()) != null) {
                    response.append(responseLine.trim());
                }
                System.out.println(response.toString());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}



```  
  
Details de la classe SimilarWordsClient()  

```  
La classe SimilarWordsClient est un programme Java qui envoie une requête GET à une URL spécifiée et imprime la réponse.  
La méthode main est le point d'entrée du programme.  
import java.net.HttpURLConnection;
import java.net.URL;
import java.io.BufferedReader;
import java.io.InputStreamReader;
Ces importations permettent d'utiliser les classes nécessaires pour gérer les connexions HTTP, lire et écrire des données sur ces connexions, et lire des données de façon tamponnée.
wordToSend est la variable qui contient le mot à envoyer au serveur.
L'URL du serveur Flask est construite en ajoutant ce mot comme paramètre de requête. Cette URL pointe vers une API Flask qui attend un mot en paramètre pour renvoyer des mots similaires.

Une connexion HttpURLConnection est ouverte à l'URL spécifiée.
La méthode de requête est définie sur GET, ce qui signifie que le programme demande des données au serveur.
Un BufferedReader est utilisé pour lire la réponse du serveur ligne par ligne.
La réponse est construite et accumulée dans un StringBuilder.
La réponse complète est finalement imprimée dans la console.
Cette partie du code capture toute exception qui pourrait survenir pendant l'exécution du programme, comme des problèmes de connexion ou des erreurs lors de la lecture ou de l'écriture des données.
Ce programme est utilisé pour interagir avec une API Flask qui prend un mot comme entrée et renvoie des mots similaires. Il peut être utilisé pour tester l'API ou pour intégrer une fonctionnalité de recherche de mots similaires dans une application Java.

```  



# Etape 5 - Améliorations des scripts  

## Amélioration du fichier similar.py -> similar2.py    

Lors de mes tests, j'ai relevé un premier problème: en effet pour le mot 'table', 'tableau' nous avions dans la liste des mots similaires retournée: 'tables', 'tableaux'.  
J'ai du modifier le script de similar.py pour ne pas envoyer dans la liste des 5 mots similaires, le mot d'origine au pluriel.  
Cette nouvelle version se trouve dans le fichier similar2.py dont le code est ci-desous:

```bash
from flask import Flask, request, jsonify
from gensim.models import KeyedVectors

app = Flask(__name__)

# Charger le modèle Word2Vec
model = KeyedVectors.load_word2vec_format('model/fr_model_skip.bin', binary=True)

@app.route('/similaire', methods=['GET'])
def get_similar_words():
    word = request.args.get('word', '')
    print(f"Mot reçu: {word}")
    if not word:
        return "Aucun mot fourni", 400

    try:
        similar_words = []
        topn = 5  # Nombre initial de mots similaires à chercher
        while len(similar_words) < 5:
            for similar_word, _ in model.most_similar(word, topn=topn):
                # Vérifier si le mot similaire n'est pas le mot d'origine au pluriel
                if similar_word != word + 's' and similar_word != word + 'x' and similar_word not in similar_words:
                    similar_words.append(similar_word)
                if len(similar_words) == 5:
                    break
            topn += 5  # Augmenter le nombre de mots à chercher si nécessaire

        similar_words_str = ', '.join(similar_words)
        return similar_words_str
    except KeyError as e:
        return str(e), 404

if __name__ == '__main__':
    app.run(ssl_context=('/usr/local/psa/var/modules/letsencrypt/etc/live/olivier-fabre.com/fullchain.pem',
                         '/usr/local/psa/var/modules/letsencrypt/etc/live/olivier-fabre.com/privkey.pem'),
            host='0.0.0.0', port=5000)


```

### Explications du code  

```
- similar_words = []: Initialise une liste vide pour stocker les mots similaires.  
- topn = 5: Définit le nombre initial de mots similaires à rechercher.  
- while len(similar_words) < 5:: Une boucle qui continue jusqu'à ce que 5 mots similaires uniques soient trouvés.  
- for similar_word, _ in model.most_similar(word, topn=topn): Parcourt les mots similaires au mot recherché, obtenus à partir du modèle Word2Vec (model.most_similar). topn spécifie le nombre de résultats à retourner. 
- if similar_word != word + 's' and similar_word != word + 'x' and similar_word not in similar_words: Vérifie si le mot similaire n'est pas une forme plurielle (ajout de 's' ou 'x') ou déjà dans la liste similar_words.  
- similar_words.append(similar_word): Ajoute le mot similaire à la liste s'il passe le filtre.
- if len(similar_words) == 5: break: Sort de la boucle si 5 mots uniques ont été trouvés.
- topn += 5: Augmente le nombre de résultats à rechercher si moins de 5 mots uniques ont été trouvés dans l'itération actuelle.


```


## Amélioration sur le fichier similar2.py -> similar3.py   

J'ai relevé un second problème: en effet pour la recherche du mot 'bateau', 'fete' nous avions dans la liste des mots similaires 'bâteau', 'fête'.  
j'ai du modifier une seconde fois le script de façon à ne pas retourner ces mots mais au final toujours envoyé 5 mots le splus simialires.  
Cette nouvelle version se trouve dans le fichier similar2.py dont le code est ci-desous:

```bash
from flask import Flask, request, jsonify
from gensim.models import KeyedVectors

app = Flask(__name__)

# Charger le modèle Word2Vec
model = KeyedVectors.load_word2vec_format('model/fr_model_skip.bin', binary=True)

import unicodedata

def remove_accents(input_str):
    nfkd_form = unicodedata.normalize('NFKD', input_str)
    return "".join([c for c in nfkd_form if not unicodedata.combining(c)])

@app.route('/similaire', methods=['GET'])
def get_similar_words():
    word = request.args.get('word', '')
    print(f"Mot reçu: {word}")
    if not word:
        return "Aucun mot fourni", 400

    try:
        similar_words = []
        topn = 5
        while len(similar_words) < 5:
            for similar_word, _ in model.most_similar(word, topn=topn):
                similar_word_no_accents = remove_accents(similar_word)

                # Vérifier si le mot similaire n'est ni une forme plurielle ni une variante accentuée du mot d'origine
                if similar_word_no_accents != remove_accents(word) \
                   and similar_word_no_accents != word + 's' \
                   and similar_word_no_accents != word + 'x' \
                   and similar_word_no_accents not in similar_words:
                    similar_words.append(similar_word_no_accents)
                if len(similar_words) == 5:
                    break
            topn += 5

        similar_words_str = ', '.join(similar_words)
        return similar_words_str
    except KeyError as e:
        return str(e), 404

if __name__ == '__main__':
    app.run(ssl_context=('/usr/local/psa/var/modules/letsencrypt/etc/live/olivier-fabre.com/fullchain.pem',
                         '/usr/local/psa/var/modules/letsencrypt/etc/live/olivier-fabre.com/privkey.pem'),
            host='0.0.0.0', port=5000)



```



### Explications du code  

```
remove_accents(input_str): est une fonction en Python conçue pour enlever les accents des caractères dans une chaîne de caractères


- import unicodedata: Le module unicodedata fournit accès à la base de données Unicode, qui est une norme permettant de représenter du texte dans la plupart des systèmes d'écriture du monde. Ce module est utilisé pour travailler avec les propriétés Unicode des caractères.
- def remove_accents(input_str): Déclare une fonction nommée remove_accents qui prend un paramètre input_str. Ce paramètre représente la chaîne de caractères à partir de laquelle les accents seront enlevés.
- nfkd_form = unicodedata.normalize('NFKD', input_str): La chaîne de caractères est normalisée en utilisant la forme de normalisation NFKD (Normalization Form KD). La normalisation en NFKD décompose chaque caractère en ses composants canoniques. Par exemple, un caractère accentué comme 'é' sera décomposé en 'e' et l'accent séparé.
- return "".join([c for c in nfkd_form if not unicodedata.combining(c)]): Cette ligne de code reconstruit une chaîne de caractères à partir des composants décomposés, en excluant les caractères combinants (comme les accents). La fonction unicodedata.combining(c) retourne un vrai (True) si 'c' est un caractère combinant (comme un accent), ce qui signifie qu'il sera exclu de la chaîne finale. Le résultat est une chaîne de caractères sans accents.


```


## Amélioration sur le fichier java SimilarWordsClient.java -> SimilarWordsClient_Version2.java  

Comme nous pouvons le voir sur l'image ci-dessous, les caractères spéciaux sont mal envoyés par ma classe SimilarWordsClient en java. Ici, le mot 'stoïque'.  
![Texte alternatif](CaptureImage1.jpg "Titre facultatif")
Je dois préciser que le codage du mot est au format UTF-8 avant l'envoyer au serveur Http.
Je dois modifier le code java avec une nouvelle version qui se trouver dans SimilarWordsClient_Version2.java et le code est ci-dessous:

```java
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.io.BufferedReader;
import java.io.InputStreamReader;

public class SimilarWordsClient {
    public static void main(String[] args) {
        try {
            String word = "fête";
            // Encoder le mot pour l'inclure dans l'URL
            String encodedWord = URLEncoder.encode(word, "UTF-8");

            URL url = new URL("https://olivier-fabre.com:5000/similaire?word=" + encodedWord);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("GET");
            BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream(), "utf-8"));

            StringBuilder response = new StringBuilder();
            String line;
            while ((line = reader.readLine()) != null) {
                response.append(line.trim());
            }
            reader.close();

            System.out.println(response.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}

```


### Explications du code  

```

- l'encodage pour les URLs est légèrement différent et nécessite l'utilisation de l'encodage de pourcentage (percent encoding). On ajoute la classe java.net.URLEncoder.
- URLEncoder.encode(word, "UTF-8") est utilisé pour encoder le mot "fête" par exemple en UTF-8 de manière à ce qu'il soit correctement transmis via une URL

```




lien : https://gitlab.com/ceri-projet-programmation-2023/groupe-9/-/tree/partie_python/java  






