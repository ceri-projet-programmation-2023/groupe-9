My responsibilities encompassed a series of well-defined tasks. Initially, I visited the site http://www.lexique.org/shiny/openlexicon/ with the purpose of extracting all French words in Excel format. The use of Excel proved to be particularly helpful in analyzing and extracting the words.

Subsequently, I proceeded to convert all the words into a text (txt) format. This step was crucial to extract specifically the words of 5, 6, 7, and 8 letters, with each category of words saved in a separate file. These files, resulting from the data manipulation and filtering, are available on GitLab.

Regarding the technical aspect of the extraction, I opted for the use of the C programming language. For those interested in the implementation of this extraction, the source code is made available on GitLab.

In addition to the technical aspect, I also took charge of drafting the game manual. The manual aims to provide users with clear and concise instructions to fully enjoy the gaming experience.

For those who wish to explore my work in more detail, all of my contributions can be accessed through my branch on GitLab: https://gitlab.com/ceri-projet-programmation-2023/groupe-9/-/tree/amadou?ref_type=heads. There, you will find not only the source code related to data extraction but also other essential elements of the project.

Example excerpt from my C code for data extraction after filtering with Excel:

#include <stdio.h>
#include <string.h>
#include <ctype.h>

int main() {
    FILE *fp_in, *fp_out;
    char word[100];
    int i, valid;

    // Open the input file in read mode
    fp_in = fopen("Classeur1.txt", "r");
    if (fp_in == NULL) {
        printf("Error opening the input file\n");
        return 1;
    }

    // Open the output file in write mode
    fp_out = fopen("output.txt", "w");
    if (fp_out == NULL) {
        printf("Error opening the output file\n");
        return 1;
    }

    // Read each word from the input file
    while (fscanf(fp_in, "%s", word) != EOF) {
        valid = 1;

        // Check each character of the word
        for (i = 0; i < strlen(word); i++) {
            // If the character is not an unaccented letter, mark the word as invalid
            if (!isalpha(word[i]) || !isascii(word[i])) {
                valid = 0;
                break;
            }
        }

        // If the word is valid and has 5 letters, write it to the output file
        if (valid && strlen(word) == 5) {
            fprintf(fp_out, "%s\n", word);
        }
    }

    // Close the files
    fclose(fp_in);
    fclose(fp_out);

    return 0;
}
