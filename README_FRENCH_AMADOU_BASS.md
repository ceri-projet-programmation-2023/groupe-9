Mes responsabilités ont impliqué une série d'actions bien définies. Initialement, j'ai exploré le site http://www.lexique.org/shiny/openlexicon/ dans le but d'extraire l'ensemble des mots français, et ce, au format Excel. L'utilisation d'Excel s'est avérée particulièrement utile dans le cadre de l'analyse et de l'extraction des mots.

Par la suite, j'ai entrepris la conversion de tous ces mots au format texte (txt). Cette étape était essentielle pour pouvoir extraire spécifiquement les mots de 5, 6, 7 et 8 lettres, chaque catégorie de mots étant sauvegardée dans un fichier distinct. Ces fichiers, résultant de la manipulation et du filtrage des données, sont accessibles sur GitLab.

Concernant l'aspect technique de l'extraction, j'ai opté pour l'utilisation du langage de programmation C. Pour ceux intéressés par l'implémentation de cette extraction, le code source est mis à disposition sur GitLab.

En plus de l'aspect technique, j'ai également pris en charge la rédaction du mode d'emploi du jeu, visant à fournir aux utilisateurs des instructions claires et concises pour profiter pleinement de l'expérience de jeu.

Pour ceux qui souhaitent explorer plus en détail mon travail, l'ensemble de mes contributions est accessible via ma branche sur GitLab : https://gitlab.com/ceri-projet-programmation-2023/groupe-9/-/tree/amadou?ref_type=heads. Vous y trouverez non seulement le code source lié à l'extraction des données, mais aussi d'autres éléments essentiels au projet.

Exple d'extrait de mon code c pour l'extraction des donnees apres le filtrage avec excel :

#include <stdio.h>
#include <string.h>
#include <ctype.h>

int main() {
    FILE *fp_in, *fp_out;
    char word[100];
    int i, valid;

    // Ouvrir le fichier d'entrée en mode lecture
    fp_in = fopen("Classeur1.txt", "r");
    if (fp_in == NULL) {
        printf("Erreur lors de l'ouverture du fichier d'entrée\n");
        return 1;
    }

    // Ouvrir le fichier de sortie en mode écriture
    fp_out = fopen("output.txt", "w");
    if (fp_out == NULL) {
        printf("Erreur lors de l'ouverture du fichier de sortie\n");
        return 1;
    }

    // Lire chaque mot du fichier d'entrée
    while (fscanf(fp_in, "%s", word) != EOF) {
        valid = 1;

        // Vérifier chaque caractère du mot
        for (i = 0; i < strlen(word); i++) {
            // Si le caractère n'est pas une lettre non accentuée, marquer le mot comme invalide
            if (!isalpha(word[i]) || !isascii(word[i])) {
                valid = 0;
                break;
            }
        }

        // Si le mot est valide et a 5 lettres, l'écrire dans le fichier de sortie
        if (valid && strlen(word) == 5) {
            fprintf(fp_out, "%s\n", word);
        }
    }

    // Fermer les fichiers
    fclose(fp_in);
    fclose(fp_out);

    return 0;
}
